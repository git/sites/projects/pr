Welcome to first Gentoo interview for quarter 1, 2016. I am your host maffblaster. Today we'll be interviewing a long time Gentoo veteran, SwifT

== Swift's interview ==

Feel free to skip any questions, or add your own!

=== About swift ===

* Tell us a little about yourself: who are you, where you live, what are you doing in life, and what do you do for Gentoo? (We can get to more specifics later in the [[#Personal questions|Personal questions]] section!)
** Tell us how to say the *correct* (as said in your native tongue) pronunciation of your real name *and* your developer nick name?
** Is there a story behind your developer nick name?
* How long have you been involved with Gentoo? (More questions like this one coming later in the [[#Questions every developer should be asked|Questions every developer should be asked]] section.)

== Project involvement ==

I'd like to talk a little bit about the various projects that benefit from you work on Gentoo.

=== Documentation Project ===

* Current state of documentation in Gentoo: What needs to be focused on the most? (Translations? Higher quality documentation? More community involvement? More developer involvement? Other things?)
* Are they any topics not written on yet that you'd like to see available on the Gentoo Wiki?

=== Gentoo Hardened ===

* Tell us a little bit about the Hardened project and your involvement with it.

==== SELinux ====

* What is SELinux? Tell us a little bit about it.
* Tell us who should switch to SELinux? (Is this for everyone?)
* Is it difficult to switch SELinux? (Perhaps rate the level of complexity in switching using it?)

=== Integrity, Compliance, and Reporting ===	

* What is the Integrity, Compliance, and Reporting project?
* What is entailed in your 'role' of working on Documentation, Userspace?

=== Gentoo Foundation ===

* Tell us a little bit about what it means to be a board member for the Gentoo Foundation.
* Tell us what it is like to be part of the Foundation.

=== Release Engineering ===

* Do you still help with the Release Engineering project?
* What does it (or did it) look like to be a Documentation Liaison for RelEng?

=== ebuild developer ===

* What ebuilds do you bump most regularly?
* What is the most difficult ebuild you've ever written?

==== Personal questions ====

===== Education =====

* What are you learning right now?
* What kinds of things are you interested in learning in the future?
* Do you have any other hobbies besides hacking on Gentoo related things? (talk about whatever you enjoy!)

===== Employment =====

* Where are you currently working/what do you do for your current job?
* What is your 'dream job'?

===== Favorite things =====

* What is your favorite food? (Bonus points for national foods!)
* What is your favorite computer game?

===== Tech interests =====

* Is there anything that *excites* you about where technology is heading?
* Is there anything that *scares* you about where technology is heading?
* Do you follow any blogs or news websites that you've found particularly interesting?
* Do you use any other distributions of Linux?

=== Questions for every developer ===

==== Raising Gentoo to prominence ====

* What, if any, are some *specific* things you would like to see improved in the Gentoo project overall? (The more practical, the better! Could be things like SSO, more PR, better attitudes, etc. Feel free to spend a decent amount of time on this question!)
** Are there specific ways people or policies in the Gentoo project or community need to change? What do you think is needed in order to make this happen?
* Are there things you would like to do for Gentoo that you have not gotten to yet? (What are those things?)
* What was the most challenging project you've worked on in Gentoo?

==== Community relations ====

* Do you have a favorite/most appreciated Gentoo developer? (Someone who you typically 'keep up' with. You may enjoy reading their blog, or getting their perspective on things.)
* Who was your mentor? (Who nurtured you and brought you into the Gentoo project?)
* Have you ever mentored anyone? (Mention their real names/dev nick names and what they were doing for the project)
** Are the developers/staffers you mentored still with the Gentoo project today?
* Are you open to mentoring a new developer right now? Why or why not?

==== Developer suggestions ====

* Is there any software you find crucial to your workflow as a developer?
* Do you have any recommendations, in general, for other developers?

==== Questions to other developers ====

* What developer should be interview next? What questions to you have for that developer?
* What what kept you developing and using Gentoo over the years? What makes Gentoo special to you?


* Are there more questions that every developer should be asked? If so, what are they?

==== Questions from users ====

N/A. This is the first interview!
