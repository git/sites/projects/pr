# Gentoo Miniconf (LinuxDays) - Prague, 2016

The original videos are licensed under CC on YouTube and are openly mirrored at Archive.org.

https://creativecommons.org/licenses/by/4.0/


## YouTube

Uncut (raw): https://www.youtube.com/watch?v=rUnaSosMle8

Playlist: https://www.youtube.com/playlist?list=PLICDJo0onMRJFwAD3V7KGZWgnkxQaELeh

Gentoo Foundation, background and status report - Robin Johnson - https://www.youtube.com/watch?v=S3bmXVbxMgE

Gentoo Infra Roundtable - Robin Johnson - https://www.youtube.com/watch?v=tXVIouIS48w

Gentoo EAPI 7 - Ulrich Müller - https://www.youtube.com/watch?v=AMMa0dCavzw

Contributing Gentoo through Proxy Maintainers Project - Göktürk Yüksek - https://www.youtube.com/watch?v=2fvIICPZBcw

NeuroGentoo - Christian Horea - https://www.youtube.com/watch?v=7PAm6ND4S-E


## Archive.org

Uncut (raw): N/A. Too large to upload!

Gentoo Foundation, background and status report - Robin Johnson - https://archive.org/download/GentooMiniconflinuxdays-Prague2016/Gentoo_Foundation_background_and_status_report_Robin_Johnson-S3bmXVbxMgE.mp4

Gentoo Infra Roundtable - Robin Johnson - https://archive.org/download/GentooMiniconflinuxdays-Prague2016/Gentoo_Infra_Roundtable-Robin_Johnson-tXVIouIS48w.mp4

Gentoo EAPI 7 - Ulrich Müller - https://archive.org/download/GentooMiniconflinuxdays-Prague2016/Gentoo_EAPI_7-Ulrich_Muller-AMMa0dCavzw.mp4

Contributing Gentoo through Proxy Maintainers Project - Göktürk Yüksek - https://archive.org/download/GentooMiniconflinuxdays-Prague2016/Contributing_Gentoo_through_Proxy_Maintainers_Project-Gokturk_Yuksek-2fvIICPZBcw.mp4

NeuroGentoo - Christian Horea - https://archive.org/download/GentooMiniconflinuxdays-Prague2016/NeuroGentoo-Christian_Horea-7PAm6ND4S-E.mp4



## Sides

Gentoo Foundation, background and status report - Robin Johnson - https://projects.gentoo.org/pr/presentations/2016-10-08-linuxdays/Gentoo_Foundation_Status-Robin_Johnson.pdf

Gentoo Infra Roundtable - Robin Johnson - https://projects.gentoo.org/pr/presentations/2016-10-08-linuxdays/Gentoo_Infra_Roundtable-Robin_Johnson.pdf

Gentoo EAPI 7 - Ulrich Müller - https://projects.gentoo.org/pr/presentations/2016-10-08-linuxdays/Gentoo_EAPI7-Ulrich_M%C3%BCller.pdf

Contributing Gentoo through Proxy Maintainers Project - Göktürk Yüksek - https://projects.gentoo.org/pr/presentations/2016-10-08-linuxdays/Contributing_Gentoo_through_Proxy_Maintainers_Project-G%C3%B6kt%C3%BCrk_Y%C3%BCksek.pdf

NeuroGentoo - Christian Horea - https://projects.gentoo.org/pr/presentations/2016-10-08-linuxdays/NeuroGentoo-Christian_Horea.pdf
